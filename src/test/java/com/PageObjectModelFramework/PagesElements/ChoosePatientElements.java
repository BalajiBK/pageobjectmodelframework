package com.PageObjectModelFramework.PagesElements;

import com.PageObjectModelFramework.Utility.FindBy;

public class ChoosePatientElements {
	  public static FindBy BtnAddPatient = FindBy.FindByXpath("//input[contains(@name,'btnAddPatient')]", "BtnAddPatient");
      public static FindBy BtnSearchPtn = FindBy.FindByXpath("//input[@id='ctl00_ContentPlaceHolder1_PatientSearch_btnSearch']", "BtnSearchPtn");
      public static FindBy BtnSelectMed = FindBy.FindByXpath("//input[@id='ctl00_ContentPlaceHolder1_btnFavorite' and contains(@value,'Select Med')]", "BtnSelectMed");
      public static FindBy BtnReviewHistory = FindBy.FindByXpath("//input[@id='ctl00_ContentPlaceHolder1_btnReviewHistory']", "BtnReviewHistory");
      public static FindBy BtnSelectDx = FindBy.FindByXpath("//input[contains(@id,'btnSelectDx')]", "BtnSelectDx");
      public static FindBy BtnCheckIn = FindBy.FindByXpath("//input[contains(@id,'btnCheckIn')]", "BtnCheckIn");
      public static FindBy BtnNewRx = FindBy.FindByXpath("//input[contains(@id,'btnNewRx')]", "BtnNewRx");
      public static FindBy BtnPatientSearchMenuDrop = FindBy.FindByXpath("//img[@id='ctl00_ContentPlaceHolder1_PatientSearch_Img1']", "BtnPatientSearchMenuDrop");
      public static FindBy BtnSetSupervisingProvider = FindBy.FindByXpath("//input[contains(@id,'btnSetSupervisingProvider')]", "BtnSetSupervisingProvider");
      public static FindBy BtnSupervisingOverlayCancel = FindBy.FindByXpath("//input[contains(@id,'btnCancel')]", "BtnSupervisingOverlayCancel");
      public static FindBy BtnDocumentVisit = FindBy.FindByXpath("//input[contains(@id,'btnDocumentVisit')]", "BtnDocumentVisit");
      public static FindBy BtnReconSave = FindBy.FindByXpath("//input[@value='Save']", "BtnReconSave");
      public static FindBy BtnReconCancel = FindBy.FindByXpath("//input[contains(@id,'btnMedRecCancel')]", "BtnReconCancel");
      public static FindBy BtnReconSavePrescribe = FindBy.FindByXpath("//input[contains(@id,'ucPatientMedAndAllergyReconcile_btnSavePrescribe')]", "BtnReconSavePrescribe");

      public static FindBy RdBtnTransferOfCareEncounter = FindBy.FindByXpath("//input[contains(@id,'ucPatientMedAndAllergyReconcile_rblActionType_0')]", "RdBtnTransferOfCareEncounter");
      public static FindBy RdBtnRelevantEncounter = FindBy.FindByXpath("//input[contains(@id,'ucPatientMedAndAllergyReconcile_rblActionType_1')]", "RdBtnRelevantEncounter");

      public static FindBy EdLastName = FindBy.FindByXpath("//input[@id='ctl00_ContentPlaceHolder1_PatientSearch_txtLastName']", "EdLastName");
      public static FindBy EdFirstName = FindBy.FindByXpath("//input[contains(@id,'PatientSearch_txtFirstName')]", "EdFirstName");
      public static FindBy EdPatientID = FindBy.FindByXpath("//input[contains(@id,'PatientSearch_txtPatientID')]", "EdPatientID");
      public static FindBy EdPatientDOB = FindBy.FindByXpath("//input[contains(@id,'PatientSearch_rdiDOB')]", "EdPatientDOB");
      
      public static String _TblSearchPatientResults = "//*[contains(@id,'ctl00_ContentPlaceHolder1_grdViewPatients')]";
      
                      
      public static FindBy MsgScriptPad = FindBy.FindByXpath(".//*[@id='ctl00_ContentPlaceHolder1_ucMessage_divMessage']", "MsgScriptPad");

      public static FindBy LnkSettings = FindBy.FindByXpath("//a[contains(@id,'settingsLink')]", "LnkSettings");
      public static FindBy LnkPatients = FindBy.FindByXpath("//a[contains(@id,'patientsLink')]", "LnkPatients");
      public static FindBy LnkTasks = FindBy.FindByXpath("//a[contains(@id,'tasksLink')]", "LnkTasks");
      public static FindBy LnkLibrary = FindBy.FindByXpath("//a[contains(@id,'libraryLink')]", "LnkLibrary");
      public static FindBy LnkReports = FindBy.FindByXpath("//a[contains(@id,'reportsLink')]", "LnkReports");
      public static FindBy LnkTools = FindBy.FindByXpath("//a[contains(@id,'toolsLink')]","LnkTools");
      public static FindBy LnkMyeRx = FindBy.FindByXpath("//a[contains(@id,'myerxLink')]","LnkMyeRx");

      // commented for UI changes purpose
      //public static FindBy LnkChangeSite = FindBy.FindByXpath("//input[contains(@id,'btnSites')]", "LnkChangeSite");
      //public static FindBy LnkEditMailPharm = FindBy.FindByXpath("//input[@id='ctl00_lnkEditMOPharm']", "LnkEditMailPharm");
      //public static FindBy LnkEditRetailPharm = FindBy.FindByXpath("//input[@id='ctl00_lnkEditPharmacy']", "LnkEditRetailPharm");
      //public static FindBy LnkEditUser = FindBy.FindByXpath(".//input[contains(@id,'lnkProfile')]", "LnkEditUser");
      //public static FindBy LnkEditPatient = FindBy.FindByXpath("//input[@id='ctl00_lnkEditPatient']", "LnkEditPatient");
      //public static FindBy LnkEditAllergy = FindBy.FindByXpath("//input[contains(@id,'lnkAlleryEdit')]", "LnkEditAllergy");
      //public static FindBy LnkEditProblem = FindBy.FindByXpath("//input[contains(@id,'lbEditDx')]", "LnkEditProblem");

      public static FindBy LblLoginUserName = FindBy.FindByXpath("//span[contains(@id,'lblUser')]", "LblLoginUserName");
      public static FindBy LblSiteName = FindBy.FindByXpath("//span[contains(@id,'lblSiteName')]", "LblSiteName");
      public static FindBy LblPatientName = FindBy.FindByXpath("//span[@id='ctl00_lblPatientName']", "LblPatientName");
      public static FindBy LblOverlayMedAllergyRecon = FindBy.FindByXpath("//div[contains(@id,'ucPatientMedAndAllergyReconcile_div8')]/div[1]", "LblOverlayMedAllergyRecon");
      public static FindBy LblNoMedAllergyRecon = FindBy.FindByXpath("//div[contains(@id,'ucPatientMedAndAllergyReconcile_divReconcileWarning')]", "LblNoMedAllergyRecon");
      public static FindBy LblCheckinMessage = FindBy.FindByXpath("//div[contains(@id,'ucMessage2_divMessage')]","LblCheckinMessage");
      public static FindBy LblSelectSupervisingProviderOverlayMessage = FindBy.FindByXpath("//div[@id='ctl00_ContentPlaceHolder1_panelSetSupervisingProvider']/table/tbody/tr[1]/td", "LblSelectSupervisingProviderOverlayMessage");
      public static FindBy LblMedAllergyStatusBar = FindBy.FindByXpath("//div[contains(@id,'ucMedRecMessage')]", "LblMedAllergyStatusBar");
      public static FindBy LblPatientSearchError = FindBy.FindByXpath("//span[contains(@id,'PatientSearch_spanSearchError')]", "LblPatientSearchError");

      public static FindBy LstSelectProvider = FindBy.FindByXpath("//select[contains(@id,'ddlProvider')]", "LstSelectProvider");
      public static FindBy LstSelectSupervisingProviderFromOverlay = FindBy.FindByXpath("//select[contains(@id,'ddlSupervisingProvider')]", "LstSelectSupervisingProviderFromOverlay");
      public static String LstPatientSearchType = "//div[@id='ctl00_ContentPlaceHolder1_PatientSearch_searchTypeMenu_detached']/ul/li";
              
      public static FindBy ImgMessageQueue = FindBy.FindByXpath("//img[@id='messageQueueIcon']", "ImgMessageQueue");
      //public static FindBy ImgLogout = FindBy.FindByXpath("//img[@id='logoutIcon']", "ImgLogout");

      //For the purpose of checking latest UI changes
      public static FindBy ImgLogout = FindBy.FindByXpath("//a[@id='ctl00_lnkLogout']", "ImgLogout");
      public static FindBy LnkChangeSite = FindBy.FindByXpath("//input[contains(@id,'btnSites')]", "LnkChangeSite");
      public static FindBy LnkEditMailPharm = FindBy.FindByXpath("//span[@id='ctl00_lblPrefMOP']", "LnkEditMailPharm");
      public static FindBy LnkEditRetailPharm = FindBy.FindByXpath("//span[@id='ctl00_lblLastPharmacyName']", "LnkEditRetailPharm");
      public static FindBy LnkEditUser = FindBy.FindByXpath(".//input[contains(@id,'lnkProfile')]", "LnkEditUser");

      public static FindBy LnkEditPatient = FindBy.FindByXpath("//span[@id='ctl00_lblPatientFirst']", "LnkEditPatient");

      public static FindBy LnkEditAllergy = FindBy.FindByXpath("//span[@id='ctl00_lblAllergy']", "LnkEditAllergy");
      public static FindBy LnkEditProblem = FindBy.FindByXpath("//span[@id='ctl00_lblDx']", "LnkEditProblem");

}
