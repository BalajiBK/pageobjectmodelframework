package com.PageObjectModelFramework.Tests;

import java.util.HashMap;

import org.testng.annotations.Test;

import com.PageObjectModelFramework.Utility.TestDataProvider;

public class TestingDataprovider {
  @Test(dataProvider="getData",dataProviderClass=TestDataProvider.class)
  public void f(HashMap<String, String> testData) {
	  HashMap<String, HashMap<String,String>> userInfor = TestDataProvider.getLogiInformation();
	  System.out.println(userInfor.get("QASSOSHLDON").get("SSOProviderUN"));
	  System.out.println(testData.get("iterationBrowser"));
  }
}
