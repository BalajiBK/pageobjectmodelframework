package com.PageObjectModelFramework.Utility;

public class Constants {
	public static final String CONFIGPROP_PATH = System.getProperty("user.dir")+"//Configurations//testconfig.properties";
	public static final String CHROMEDRIVER_PATH = System.getProperty("user.dir")+"//Configurations//chromedriver.exe";
	public static final long PAGELOADTIMEOUT = 20;
	public static final long ELEMENTVISIBILITYTIMEOUT = 20;
	
	public static final String TESTDATA_PATH = System.getProperty("user.dir")+"//src//test//testdata";
	public static final String COMMONTESTDATA_PATH = System.getProperty("user.dir")+"//src//test//Commontestdata";
	
	public static final String USERSDATA_PATH = System.getProperty("user.dir")+"//src//test//Commontestdata//ShieldOnUsers.xml";
	
	

}
