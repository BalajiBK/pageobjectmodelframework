package com.PageObjectModelFramework.Rough;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;


import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class getData {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		String sectionName ="";
		
		HashMap<String, String> testData;
		String keyName;
		String Correspondingvalue;
		String iterationNumber="";
		String iterationrunMode="";
		String iterationBrowser="";
		int objectArrcount=0;
		int numberOfIteration;
		
      try {
    	  numberOfIteration =getNumberOfRunnableIteration();
		DocumentBuilder Dbldr = DocumentBuilderFactory.newInstance().newDocumentBuilder();
		Object[][] objectArr = new Object[1][numberOfIteration];
		
	   Document xmldoc =Dbldr.parse(new File("C://Users//bbawakesavan//workspace//PageObjectModelFramework//src//test//testdata//ScriptPad_AddSingleScript.xml"));
		NodeList lst = xmldoc.getChildNodes();
		NodeList chldlst;
		NodeList sectionchld;
		for(int i=0;i<lst.getLength();i++)
		{
		System.out.println(lst.item(i).getNodeName());
		
		chldlst = lst.item(i).getChildNodes();
		for(int j=0;j<numberOfIteration;j++)
		{
			if(chldlst.item(j).getNodeName().equalsIgnoreCase("iteration"))
			{
				System.out.println(chldlst.item(j).hasAttributes());
				try{
					if(chldlst.item(j).hasAttributes())
					{
						iterationNumber = chldlst.item(j).getAttributes().getNamedItem("number").getTextContent();
						iterationrunMode = chldlst.item(j).getAttributes().getNamedItem("runmode").getTextContent();
						iterationBrowser = chldlst.item(j).getAttributes().getNamedItem("browser").getTextContent();
						System.out.println(iterationNumber);
					}
				}catch(NullPointerException ex)
				{
					ex.printStackTrace();
				}
			}
			if(iterationrunMode.equalsIgnoreCase("Y"))
			{
				testData = new HashMap<String,String>();
				testData.put("iterationNumber", iterationNumber);
				testData.put("iterationrunMode", iterationrunMode);
				testData.put("iterationBrowser", iterationBrowser);
				sectionchld = chldlst.item(j).getChildNodes();
				for(int k=0;k<sectionchld.getLength();k++)
				{
					
					if(sectionchld.item(k).hasAttributes())
					{
						if(sectionchld.item(k).getAttributes().getLength()==2)
						{
							keyName = sectionchld.item(k).getAttributes().getNamedItem("key").getTextContent();
							Correspondingvalue = sectionchld.item(k).getAttributes().getNamedItem("value").getTextContent();
							System.out.println(keyName);
							System.out.println(Correspondingvalue);
							
							testData.put(keyName, Correspondingvalue);
						}
					}
					
				}
				objectArr[0][objectArrcount] = testData;
				objectArrcount++;
			}
			
			
		}
		}
	} catch (ParserConfigurationException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} catch (SAXException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
     
	}
	
	public static int getNumberOfRunnableIteration() throws ParserConfigurationException, SAXException, IOException
	{
		int NumberOfIterations=0;
		String iterationNumber="";
		String iterationrunMode="";
		String iterationBrowser="";
		DocumentBuilder Dbldr = DocumentBuilderFactory.newInstance().newDocumentBuilder();
	    Document xmldoc =Dbldr.parse(new File("C://Users//bbawakesavan//workspace//PageObjectModelFramework//src//test//testdata//ScriptPad_AddSingleScript.xml"));
			NodeList lst = xmldoc.getChildNodes();
			NodeList chldlst;
			NodeList sectionchld;
			for(int i=0;i<lst.getLength();i++)
			{
				System.out.println(lst.item(i).getNodeName());
				chldlst = lst.item(i).getChildNodes();
				
				for(int j=0;j<chldlst.getLength();j++)
				{
					if(chldlst.item(j).getNodeName().equalsIgnoreCase("iteration"))
					{
							if(chldlst.item(j).hasAttributes())
							{
								iterationNumber = chldlst.item(j).getAttributes().getNamedItem("number").getTextContent();
								iterationrunMode = chldlst.item(j).getAttributes().getNamedItem("runmode").getTextContent();
								iterationBrowser = chldlst.item(j).getAttributes().getNamedItem("browser").getTextContent();
								System.out.println(iterationNumber);
							}
							if(iterationrunMode.equalsIgnoreCase("y"))
							{
								NumberOfIterations++;
							}
						
					}
				}
			}
			System.out.println("NumberOfIterations  "+NumberOfIterations);
			Dbldr=null;
			xmldoc = null;
			return NumberOfIterations;
	}

}
