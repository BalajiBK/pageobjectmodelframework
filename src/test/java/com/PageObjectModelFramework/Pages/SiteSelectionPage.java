package com.PageObjectModelFramework.Pages;


import java.util.concurrent.TimeUnit;

import com.PageObjectModelFramework.Base.PageObject;
import com.PageObjectModelFramework.PagesElements.ChoosePatientElements;
import com.PageObjectModelFramework.PagesElements.LoginElements;
import com.PageObjectModelFramework.Utility.SeleniumAutomationDriver;

public class SiteSelectionPage extends PageObject {
	
	private SiteSelectionPage()
	{
		
	}
	
    public static SiteSelectionPage Instance = new SiteSelectionPage();
	
	public SiteSelectionPage SelectSite()
	{
		System.out.println("Select the Site");
		return Instance;
		
	}

	public LoginPage Logout()
    {
        try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        SeleniumAutomationDriver.WaitTillThePageLoadCompletes();
        SeleniumAutomationDriver.WaitTillElementVisible(ChoosePatientElements.ImgLogout, 20);
        SeleniumAutomationDriver.clickOnObject(ChoosePatientElements.ImgLogout);
        SeleniumAutomationDriver.WaitTillElementVisible(LoginElements.Username, 20);
        return LoginPage.Instance;
    }
}
