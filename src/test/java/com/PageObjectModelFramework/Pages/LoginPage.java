package com.PageObjectModelFramework.Pages;

import java.lang.reflect.Field;

import org.openqa.selenium.By;


import com.PageObjectModelFramework.Base.PageObject;
import com.PageObjectModelFramework.PagesElements.LoginElements;
import com.PageObjectModelFramework.Utility.SeleniumAutomationDriver;

public class LoginPage extends PageObject {

	private LoginPage()
	{
		
	}
	
    public static LoginPage Instance = new LoginPage();

	
	public static <T extends PageObject> T ClickOnAnyObjectAndNavigate(T pg,String ObjectName) 
	{
	  	
		
      return  pg;
	}
//	
	public  SiteSelectionPage LoginToApplication(String UserNameKey,String PasswordKey)
	{
		driver.navigate().to(configprop.getProperty(appEnvironment));
		SeleniumAutomationDriver.EnterText(LoginElements.Username, LoginInfor.get(UserNameKey));
		SeleniumAutomationDriver.EnterText(LoginElements.Password, LoginInfor.get(PasswordKey));
//		driver.findElement(By.id("txtUserName")).sendKeys("FirstIntProv");
//		driver.findElement(By.id("txtPassword")).sendKeys("Selenium@123");
		driver.findElement(By.xpath("//input[@id='btnLogin']")).click();
		return SiteSelectionPage.Instance;
	}

}
