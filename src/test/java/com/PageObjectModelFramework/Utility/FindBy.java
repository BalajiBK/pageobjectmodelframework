package com.PageObjectModelFramework.Utility;

import org.openqa.selenium.By;

public class FindBy  {
	private String SearchByString;
	private String ObjectName;
	private By TheBy;
	
	public String getSearchString()
	{
		return SearchByString;
	}
	
	public String getObjectName()
	{
		return ObjectName;
	}
	
	public By getByObject()
	{
		return TheBy;
	}
	
	private FindBy(String SearchByString,String ObjectName,By By)
	{
		this.SearchByString = SearchByString;
		this.ObjectName = ObjectName;
		this.TheBy = By;
	}
	public static FindBy FindByXpath(String xpath,String objectName)
	{
		return new FindBy(xpath, objectName, By.xpath(xpath));
	}
    
	public static FindBy FindByID(String ID,String objectName)
	{
		return new FindBy(ID, objectName, By.id(ID));
	}
	
	public static FindBy findByCSSLocater(String CSSLocater,String objectName)
	{
		return new FindBy(CSSLocater, objectName, By.cssSelector(CSSLocater));
	}
	
	public static FindBy findByName(String Name,String objectName)
	{
		return new FindBy(Name, objectName, By.name(Name));
	}

	public static FindBy findByClassName(String ClassName,String objectName)
	{
		return new FindBy(ClassName, objectName, By.className(ClassName));
	}
}
