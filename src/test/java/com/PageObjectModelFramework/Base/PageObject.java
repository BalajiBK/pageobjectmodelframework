package com.PageObjectModelFramework.Base;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.firefox.internal.ProfilesIni;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;

import com.PageObjectModelFramework.Utility.Constants;
import com.PageObjectModelFramework.Utility.SeleniumAutomationDriver;
import com.PageObjectModelFramework.Utility.TestDataProvider;

public class PageObject {
	
	public static WebDriver driver;
	public static Properties configprop;
	static FileInputStream  FlisConfig;
	public static String appEnvironment;
	
	
	
	private static HashMap<String, HashMap<String,String>> userInfor = TestDataProvider.getLogiInformation();
	public  static HashMap<String,String> LoginInfor = new HashMap<String,String>();
	private static HashMap<String, WebDriver> browserMap = new HashMap<String, WebDriver>();
	public static int numberOfInstancesOfClass;
	
	static
	{
		try {
			System.out.println("Static block");
			FlisConfig = new FileInputStream(Constants.CONFIGPROP_PATH);
			configprop = new Properties();
			configprop.load(FlisConfig);
			String environmentname = configprop.getProperty("Environment");
			String ApplicationUrl;
			LoginInfor = userInfor.get(environmentname);
			
			
			int rightlength = environmentname.length()-6;
			ApplicationUrl = environmentname.replace(environmentname.substring(rightlength), "");
			
			appEnvironment = ApplicationUrl;
			browserMap.put("FIREFOX",null);
			browserMap.put("CHROME",null);
			browserMap.put("IE",null);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public PageObject()
	{ 
		numberOfInstancesOfClass++;
		System.out.println("Number of instances of pageobject "+numberOfInstancesOfClass);		
	}
	
	public static void OpenBrowser(String browserName)
	{		
		browserName = browserName.toUpperCase();
		if(browserMap.get(browserName)==null)
		{
			switch (browserName.toUpperCase().trim()) {
			
			case "FIREFOX":
				ProfilesIni prof = new ProfilesIni();
				FirefoxProfile ffpro = prof.getProfile("ForSelenium");
				driver = new FirefoxDriver(ffpro);
				
				break;
			case "CHROME":
				ChromeOptions chrmopt = new ChromeOptions();
				chrmopt.addArguments("--disable-print-preview");
				chrmopt.addArguments("--disable-popup-blocking");
	          System.setProperty("webdriver.chrome.driver", Constants.CHROMEDRIVER_PATH);
	          driver = new ChromeDriver(chrmopt);
	          break;
			case "IE":
				DesiredCapabilities cpblties = DesiredCapabilities.internetExplorer();
				cpblties.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true);
				System.setProperty("webdriver.ie.driver", Constants.CHROMEDRIVER_PATH);
				driver = new InternetExplorerDriver(cpblties);
				break;
			default:
				break;
			}
			driver.manage().timeouts().pageLoadTimeout(Constants.PAGELOADTIMEOUT, TimeUnit.SECONDS);
			driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			driver.manage().window().maximize();
			browserMap.put(browserName, driver);
		}else
		{
			driver = browserMap.get(browserName);
		}
	}
}
