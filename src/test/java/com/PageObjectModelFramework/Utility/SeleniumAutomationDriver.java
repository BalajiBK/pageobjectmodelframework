package com.PageObjectModelFramework.Utility;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.interactions.Keyboard;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import com.PageObjectModelFramework.Base.PageObject;
import com.google.common.base.Function;

public class SeleniumAutomationDriver {
	
//	public static  WebDriver driver = PageObject.driver;
	
	
	

	 public static Boolean EnterText(FindBy TextObject, String TextToEnter) 
     {
		 By ByObject = TextObject.getByObject();
         String TempStr;
         Boolean Status;
         try
         {
        	 WaitTillElementVisible(TextObject, 20);
        	 PageObject.driver.findElement(ByObject).click();
        	 PageObject.driver.findElement(ByObject).clear();
        	 PageObject.driver.findElement(ByObject).sendKeys(TextToEnter);
             

             //Re-Verify the string is properly set in the text field
             TempStr = PageObject.driver.findElement(ByObject).getAttribute("value");
             if (TextToEnter.equalsIgnoreCase(TempStr))
             {
                 System.out.println("Enter text method "+"The text " + TextToEnter + " was entered in the object :" + TextObject.getObjectName());
                 Status = true;

             }

             else
             {
                 Status = false;
//                 Validate.Fail("Enter text"+
//                     "The text " + TextToEnter + " was not entered properly in the object :" + TextObject.ElementName +
//                     " The actual value in the edit box is :" + TempStr, true);
             }
         }
         catch (Exception e)
         {
             Status = false;
//             Validate.Fail("Enter text"+"The text " + TextToEnter + " was not entered in the object :" + TextObject.ElementName + " The exception message is :" + e.ToString(),true);
             
         }
         return Status;
     }
	 
	 public static Boolean WaitTillElementVisible(FindBy TheObject,int timeToWait)
	 {
		 Boolean status = false;
		 try{				 
			 WebDriverWait wt = new WebDriverWait(PageObject.driver, Constants.ELEMENTVISIBILITYTIMEOUT);
			 wt.until(ExpectedConditions.visibilityOfElementLocated(TheObject.getByObject()));
			 status = true;
		 }
		 catch(Exception ex)
		 {
			 ex.printStackTrace();
			 throw ex;
			 
		 }
		 return status;
	 }
	 
	 public static Boolean VerifyElementIsDisplayed(FindBy TheObject)
	 {
		 Boolean status=false;
		 try{
			 status = PageObject.driver.findElement(TheObject.getByObject()).isDisplayed();
			if(status)
			{
				System.out.println(TheObject.getObjectName()+" Displayed");
			}
			else
			{
				System.out.println(TheObject.getObjectName()+" is not Displayed");
			}
			
		 }
		 catch(Exception e)
		 {
			 System.out.println(e.getMessage());
			 throw e;
		 }
		 
		 return status;
	 }

	public static void WaitTillThePageLoadCompletes(int maxTimeout) {
		// TODO Auto-generated method stub
		try{
			JavascriptExecutor Js = (JavascriptExecutor)PageObject.driver;
			WebDriverWait wt = new WebDriverWait(PageObject.driver,maxTimeout);
			wt.until((WebDriver d)->Js.executeScript("return document.readyState").equals("complete"));
		}catch(Exception e)
		{
			e.printStackTrace();
			Assert.fail("WaitTillThePageLoadCompletes");
		}
		
	}
	
	public static void WaitTillThePageLoadCompletes() {
		// TODO Auto-generated method stub
		WaitTillThePageLoadCompletes(20);
		
	}

	public static void clickOnObject(FindBy TheObject) {
		// TODO Auto-generated method stub
		WaitTillThePageLoadCompletes(20);
		PageObject.driver.findElement(TheObject.getByObject()).click();
		
	}

	public static Boolean WaitTillOptionalElementVisible(FindBy TheObject, int maxTimeOut) {
		// TODO Auto-generated method stub
		 Boolean status = false;
		 try{
			 
			 WebDriverWait wt = new WebDriverWait(PageObject.driver, maxTimeOut);
			 wt.until(ExpectedConditions.visibilityOfElementLocated(TheObject.getByObject()));
			 status = true;
		 }
		 catch(Exception ex)
		 {
			 ex.printStackTrace();
			 
			 
		 }
		 return status;
		
	}

	public static Boolean WaitTillOptionalElementVisible(FindBy TheObject) {
		return WaitTillOptionalElementVisible(TheObject,20);
	}
	
	public static Boolean SelectItemInList(FindBy lstSelectProvider, String supervisingProviderName) {
		// TODO Auto-generated method stub
		
		return true;
	}

	public static Boolean CheckOrUncheckTheCheckBox(FindBy rdBtnActivePatient, String string) {
		// TODO Auto-generated method stub
		return true;
		
	}

	public static void VerifyTextInElement(FindBy msgScriptPad, String string) {
		// TODO Auto-generated method stub
		
	}

	public static void CloseTeaserAdvertisement() {
		// TODO Auto-generated method stub
		
	}

	public static void VerifyItemInList(FindBy lstSelectProvider, String providerName, String existOrNotExist) {
		// TODO Auto-generated method stub
		
	}

	public static void SendSpecialKeys(String keyToPass) {
		// TODO Auto-generated method stub
		Actions act = new Actions(PageObject.driver);
		switch (keyToPass.toUpperCase()) {
		case "TAB":
			act.sendKeys(Keys.TAB);
			break;

		default:
			break;
		}
		
		
	}

	public static Boolean SelectValueInReadOnlyDropDown(String lstPatientSearchType, String string) {
		// TODO Auto-generated method stub
		return null;
	}

	public static int GetColumnPosition(String _TblSearchPatientResults, String string) {
		// TODO Auto-generated method stub
		return 0;
	}

	public static int GetRowPosOfItemInTable(String _TblSearchPatientResults, String patientFirstName, int colpos) {
		// TODO Auto-generated method stub
		return 0;
	}

	public static void ClickInputObjectInTable(String _TblSearchPatientResults, int rowpos, int i, String string) {
		// TODO Auto-generated method stub
		
	}
	
	

}
