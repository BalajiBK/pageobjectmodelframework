package com.PageObjectModelFramework.Utility;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.HashMap;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.testng.annotations.DataProvider;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class TestDataProvider {
	
	
	@DataProvider(name="getTestData")
	public static Object[][] getTestData(Method m)
	{
		return getXMLData(m.getName());
	}
	
	
	public static Object[][] getXMLData(String methodName)
	{
		String sectionName ="";
		HashMap<String, String> testData;
		String keyName;
		String Correspondingvalue;
		String iterationNumber="";
		String iterationrunMode="";
		String iterationBrowser="";
		int objectArrcount=0;
		int numberOfIteration;
		Object[][] objectArr=null;
		String testXMLPath = Constants.TESTDATA_PATH+"//"+methodName+".xml";
      try {
    	  numberOfIteration =getNumberOfRunnableIteration(testXMLPath);
		DocumentBuilder Dbldr = DocumentBuilderFactory.newInstance().newDocumentBuilder();
		objectArr = new Object[numberOfIteration][1];
		
	   Document xmldoc =Dbldr.parse(new File(testXMLPath));
		NodeList lst = xmldoc.getChildNodes();
		NodeList chldlst;
		NodeList sectionchld;
		for(int i=0;i<lst.getLength();i++)
		{
//		System.out.println(lst.item(i).getNodeName());
		
		chldlst = lst.item(i).getChildNodes();
		for(int j=0;j<chldlst.getLength();j++)
		{
			if(chldlst.item(j).getNodeType()==Node.ELEMENT_NODE)
			{
			if(chldlst.item(j).getNodeName().equalsIgnoreCase("iteration"))
			{
				System.out.println(chldlst.item(j).hasAttributes());
				try{
					if(chldlst.item(j).hasAttributes())
					{
						iterationNumber = chldlst.item(j).getAttributes().getNamedItem("number").getTextContent();
						iterationrunMode = chldlst.item(j).getAttributes().getNamedItem("runmode").getTextContent();
						iterationBrowser = chldlst.item(j).getAttributes().getNamedItem("browser").getTextContent();
//						System.out.println(iterationNumber);
					}
				}catch(NullPointerException ex)
				{
					ex.printStackTrace();
				}
			}
			if(iterationrunMode.equalsIgnoreCase("Y"))
			{
				testData = new HashMap<String,String>();
				testData.put("iterationNumber", iterationNumber);
				testData.put("iterationrunMode", iterationrunMode);
				testData.put("iterationBrowser", iterationBrowser);
				sectionchld = chldlst.item(j).getChildNodes();
				for(int k=0;k<sectionchld.getLength();k++)
				{
					
					if(sectionchld.item(k).hasAttributes())
					{
						if(sectionchld.item(k).getAttributes().getLength()==2)
						{
							keyName = sectionchld.item(k).getAttributes().getNamedItem("key").getTextContent();
							Correspondingvalue = sectionchld.item(k).getAttributes().getNamedItem("value").getTextContent();
//							System.out.println(keyName);
//							System.out.println(Correspondingvalue);
							
							testData.put(keyName, Correspondingvalue);
						}
					}
					
				}
				objectArr[objectArrcount][0] = testData;
				objectArrcount++;
			}
			
			}
		}
		}
	} catch (ParserConfigurationException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} catch (SAXException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
    return objectArr;		
	}

	public static int getNumberOfRunnableIteration(String xmlFilePath) throws ParserConfigurationException, SAXException, IOException
	{
		int NumberOfIterations=0;
		String iterationNumber="";
		String iterationrunMode="";
		String iterationBrowser="";
		DocumentBuilder Dbldr = DocumentBuilderFactory.newInstance().newDocumentBuilder();
	    Document xmldoc =Dbldr.parse(new File(xmlFilePath));
			NodeList lst = xmldoc.getChildNodes();
			NodeList chldlst;
			NodeList sectionchld;
			for(int i=0;i<lst.getLength();i++)
			{
				System.out.println(lst.item(i).getNodeName());
				chldlst = lst.item(i).getChildNodes();
				
				for(int j=0;j<chldlst.getLength();j++)
				{
					if(chldlst.item(j).getNodeName().equalsIgnoreCase("iteration"))
					{
							if(chldlst.item(j).hasAttributes())
							{
								iterationNumber = chldlst.item(j).getAttributes().getNamedItem("number").getTextContent();
								iterationrunMode = chldlst.item(j).getAttributes().getNamedItem("runmode").getTextContent();
								iterationBrowser = chldlst.item(j).getAttributes().getNamedItem("browser").getTextContent();
								System.out.println(iterationNumber);
							}
							if(iterationrunMode.equalsIgnoreCase("y"))
							{
								NumberOfIterations++;
							}
						
					}
				}
			}
			System.out.println("NumberOfIterations  "+NumberOfIterations);
			Dbldr=null;
			xmldoc = null;
			return NumberOfIterations;
	}

	
	public static HashMap<String, HashMap<String,String>> getLogiInformation()
	{
		HashMap<String, String> testData;
		String keyName;
		String Correspondingvalue;
		String EnvironmentName="";
		int numberOfIteration;
		HashMap<String, HashMap<String,String>> UserInformation = new HashMap<String, HashMap<String,String>>();
		try {
    	  
		DocumentBuilder Dbldr = DocumentBuilderFactory.newInstance().newDocumentBuilder();
		Document xmldoc =Dbldr.parse(new File(Constants.USERSDATA_PATH));
		NodeList lst = xmldoc.getChildNodes();
		NodeList chldlst;
		NodeList sectionchld;
		for(int i=0;i<lst.getLength();i++)
		{
//		System.out.println(lst.item(i).getNodeName());
		
		chldlst = lst.item(i).getChildNodes();
		for(int j=0;j<chldlst.getLength();j++)
		{
			if(chldlst.item(j).getNodeType()==Node.ELEMENT_NODE)
			{
				if(chldlst.item(j).getNodeName().equalsIgnoreCase("section"))
				{
					System.out.println(chldlst.item(j).hasAttributes());
						if(chldlst.item(j).hasAttributes())
						{
							EnvironmentName = chldlst.item(j).getAttributes().getNamedItem("name").getTextContent();
						}				
				testData = new HashMap<String,String>();
				sectionchld = chldlst.item(j).getChildNodes();
				for(int k=0;k<sectionchld.getLength();k++)
				{
					
					if(sectionchld.item(k).hasAttributes())
					{
						if(sectionchld.item(k).getAttributes().getLength()==2)
						{
							keyName = sectionchld.item(k).getAttributes().getNamedItem("key").getTextContent();
							Correspondingvalue = sectionchld.item(k).getAttributes().getNamedItem("value").getTextContent();
//							System.out.println(keyName);
//							System.out.println(Correspondingvalue);
							
							testData.put(keyName, Correspondingvalue);
						}
					}
					
				}
				UserInformation.put(EnvironmentName, testData);
				}
			
			}
		}
		}
	} catch (ParserConfigurationException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} catch (SAXException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
    return UserInformation;		
	}
}
