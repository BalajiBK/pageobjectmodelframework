package com.PageObjectModelFramework.Tests;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

import org.apache.pdfbox.cos.COSDocument;

import org.apache.pdfbox.pdfparser.PDFParser;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.util.PDFTextStripper;
import org.testng.annotations.Test;

import com.PageObjectModelFramework.Utility.ReadPDF;

public class ComparePDF {
  @Test
  public void f() {
	  
	  verifyPDFContent("", "Jmeter");
  }
  
  public boolean verifyPDFContent(String strURL, String reqTextInPDF) {
		
		boolean flag = false;
		
		PDFTextStripper pdfStripper = null;
		PDDocument pdDoc = null;
		COSDocument cosDoc = null;
		String parsedText = null;

		try {
//			URL url = new URL(strURL);
//			BufferedInputStream file = new BufferedInputStream(url.openStream());
//			PDFParser parser = new PDFParser((RandomAccessRead) file);
			
			FileInputStream flins = new FileInputStream(new File("C:\\Users\\bbawakesavan\\Desktop\\Balaji_AutomationtestEngineer.pdf"));
			PDFParser parser = new PDFParser(flins);
			parser.parse();
			cosDoc = parser.getDocument();
			
			pdfStripper = new PDFTextStripper();
			pdfStripper.setStartPage(2);
			pdfStripper.setEndPage(2);
			
			pdDoc = new PDDocument(cosDoc);
			parsedText = pdfStripper.getText(pdDoc);
			System.out.println(parsedText);
		} catch (MalformedURLException e2) {
			System.err.println("URL string could not be parsed "+e2.getMessage());
		} catch (IOException e) {
			System.err.println("Unable to open PDF Parser. " + e.getMessage());
			try {
				if (cosDoc != null)
					cosDoc.close();
				if (pdDoc != null)
					pdDoc.close();
			} catch (Exception e1) {
				e.printStackTrace();
			}
		}
		
		System.out.println("+++++++++++++++++");
		System.out.println(parsedText);
		System.out.println("+++++++++++++++++");

		if(parsedText.contains(reqTextInPDF)) {
			flag=true;
		}
		
		return flag;
	}
}
