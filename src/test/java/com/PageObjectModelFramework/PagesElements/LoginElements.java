package com.PageObjectModelFramework.PagesElements;

import com.PageObjectModelFramework.Utility.FindBy;

public class LoginElements {

	public static FindBy Username = FindBy.FindByID("txtUserName", "UserName_txt");
	public static FindBy Password = FindBy.FindByID("txtPassword", "Password_txt");
	public static FindBy Btnlogin = FindBy.FindByXpath("//input[@id='btnLogin']", "Btnlogin");
    public static FindBy BtnContinue = FindBy.FindByXpath("//input[@id='btnContinue']", "BtnContinue");

}
