package com.PageObjectModelFramework.Tests;

import java.util.HashMap;

import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;

import com.PageObjectModelFramework.Base.PageObject;
import com.PageObjectModelFramework.Pages.LoginPage;
import com.PageObjectModelFramework.Utility.TestDataProvider;

public class VerifyLogins {
	
	
  @Test(dataProvider="getTestData",dataProviderClass=TestDataProvider.class,priority=0)
  public void LoginAsProvider(HashMap<String, String> inputData) {
	  
	  
	  LoginPage.OpenBrowser(inputData.get("iterationBrowser"));
	  LoginPage.ClickOnAnyObjectAndNavigate(LoginPage.Instance,"SignIn Button").LoginToApplication("ProviderUN","ProviderPWD")
	  .SelectSite()
	  .Logout();  
	  
	  
  }
  
  @Test(dataProvider="getTestData",dataProviderClass=TestDataProvider.class,priority=1)
  public void LoginAsPA(HashMap<String, String> inputData) {
	  
	  
	  LoginPage.OpenBrowser(inputData.get("iterationBrowser"));
	  LoginPage.ClickOnAnyObjectAndNavigate(LoginPage.Instance,"SignIn Button").LoginToApplication("PAWOSupUN","PAWOSupPWD")
	  .SelectSite()
	  .Logout();  
	  
	  
  }
  
  @AfterTest  
  public void tearDown()
  {
	  PageObject.driver.quit();
  }
}
